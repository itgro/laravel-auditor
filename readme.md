# Установка

1. Добавить в composer.json:
```
"repositories": [
    ...
    {
        "type": "vcs",
        "url": "https://gitlab.com/itgro/laravel-auditor.git"
    }
    ...
],
```
2. Выполнить: `composer require itgro/laravel-auditor`
3. После завершения установки, в класс каждой модели, изменения которой нужно мониторить, добавить трейт `Itgro\LaravelAuditor\Audited` и задекларировать реализацию контракта `Itgro\LaravelAuditor\Auditable`.  
Регистрировать сервис-провайдер не нужно, это будет сделано автоматически.
4. Опубликовать конфиг и миграцию: `php artisan vendor:publish`
5. Изменить timestamp миграции на более ранний, если у вас уже есть миграции, в которых через Eloquent создаются модели. Иначе миграция будет падать с ошибкой отсутствия таблицы.
6. Добавить в .env: `AUDITOR_ENABLED=true`, настроить конфиг
7. Выполнить миграцию: `php artisan migrate` или `php artisan migrate:fresh`, если меняли timestamp миграции.
