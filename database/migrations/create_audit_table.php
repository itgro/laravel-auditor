<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAuditTable extends Migration
{
    protected $table;

    public function __construct()
    {
        $this->table = config('auditor.table');
    }

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->index();
            $table->json('payload');
            $table->json('metadata');
            $table->timestamp('recorded_on');
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
