<?php
declare(strict_types=1);

namespace Itgro\LaravelAuditor;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Arr;
use Throwable;

class Auditor
{
    public function start()
    {
        try {
            $this->listenToModelEvents();
            $this->listenToPivotEvents();
        } catch (Throwable $e) {
            // Do nothing
        }
    }

    private function listenToModelEvents(): void
    {
        /** @var Dispatcher $events */
        $events = app('events');

        $events->listen(
            'eloquent.saved: *',
            function (string $eventName, array $data): void {
                /** @var Model $model */
                $model = Arr::first($data);

                if (!$this->shouldLog($eventName, $model)) {
                    return;
                }

                $before = $model->getRawOriginal();
                // Здесь нельзя использовать $model->toArray(), потому что нам не нужны вычисляемые поля и связанные сущности.
                $after = $model->getAttributes();

                $changes = array_merge(array_diff($before, $after), array_diff($after, $before));

                if (empty($changes) && config('auditor.ignore_empty_saves', false)) {
                    return;
                }

                $payload = [
                    'changed_attributes' => $changes,
                ];

                $this->logModelEvent($model, $eventName, $payload);
            }
        );

        $events->listen(
            ['eloquent.deleted: *', 'eloquent.forceDeleted: *'],
            function (string $eventName, array $data): void {
                /** @var Model $model */
                $model = Arr::first($data);

                if ($this->shouldLog($eventName, $model)) {
                    $this->logModelEvent($model, $eventName);
                }
            }
        );
    }

    private function listenToPivotEvents(): void
    {
        /** @var Dispatcher $events */
        $events = app('events');

        $events->listen(
            ['eloquent.pivotAttached: *', 'eloquent.pivotDetached: *', 'eloquent.pivotUpdated: *'],
            function (string $eventName, array $data): void {
                /** @var Model $model */
                [$model, $relation, $pivotIds, $pivotIdsAttributes] = $data;

                if (!$this->shouldLog($eventName, $model)) {
                    return;
                }

                $payload = [
                    'relation' => $relation,
                    'pivot_ids' => $pivotIds,
                    'pivot_ids_attributes' => $pivotIdsAttributes,
                ];

                $this->logModelEvent($model, $eventName, $payload);
            }
        );
    }

    private function logModelEvent(?Model $model, string $type, $payload = []): void
    {
        $payload = array_merge([
            'model_id' => $model->getKey(),
            'model_class' => relative_class_path(config('auditor.models_namespace'), $model),
        ], $payload);

        $metadata = $model->getAuditMetadata($type, $payload);

        $this->logEvent($type, $payload, $metadata);
    }

    private function logEvent(string $type, $payload, array $metadata = []): void
    {
        if (!config('auditor.enabled', false)) {
            return;
        }

        $metadata = array_merge($this->makeDefaultMetadata(), $metadata);

        $jsonOptions = config('auditor.json_encode_options');

        /** @var Connection $db */
        $db = app('db');

        // Если здесь использовать модель, то придётся фильтровать её события, иначе получится бесконечный цикл
        $db->table(config('auditor.table'))->insert([
            'type' => $type,
            'payload' => json_encode($payload, $jsonOptions),
            'metadata' => json_encode($metadata, $jsonOptions),
            'recorded_on' => Carbon::now(),
        ]);
    }

    private function shouldLog(string $event, $model): bool
    {
        if (
            !config('auditor.enabled', false) ||
            !$model ||
            !($model instanceof Auditable) ||
            !$model->shouldLog($event)
        ) {
            return false;
        }

        return true;
    }

    private function makeDefaultMetadata(): array
    {
        $defaultMetadata = [];

        if (config('auditor.metadata.env', true)) {
            $defaultMetadata['env'] = app()->environment();
        }

        if (config('auditor.metadata.running_in_console', true)) {
            $defaultMetadata['running_in_console'] = app()->runningInConsole();
        }

        if (config('auditor.metadata.user_id', true)) {
            $defaultMetadata['user_id'] = auth()->id();
        }

        if (config('auditor.metadata.user_ip', false)) {
            $defaultMetadata['user_ip'] = request()->ip();
        }

        return $defaultMetadata;
    }
}
