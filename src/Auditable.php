<?php
declare(strict_types=1);

namespace Itgro\LaravelAuditor;

interface Auditable
{
    public function shouldLog(string $event): bool;

    public function getAuditMetadata(string $type, $payload): array;
}
