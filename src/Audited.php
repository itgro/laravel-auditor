<?php
declare(strict_types=1);

namespace Itgro\LaravelAuditor;

use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;

trait Audited
{
    use PivotEventTrait;

    public function shouldLog(string $event): bool
    {
        return true;
    }

    public function getAuditMetadata(string $type, $payload): array
    {
        return [];
    }
}
