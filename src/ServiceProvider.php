<?php
declare(strict_types=1);

namespace Itgro\LaravelAuditor;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        $this->publishConfig();
        $this->publishMigrations();

        $this->registerConfig();

        (new Auditor())->start();
    }

    protected function publishConfig()
    {
        $this->publishes([
            __DIR__ . '/../config/auditor.php' => config_path('auditor.php'),
        ], 'config');
    }

    protected function publishMigrations()
    {
        if (class_exists('CreateAuditTable')) {
            return;
        }

        $timestamp = date('Y_m_d_His', time());

        $this->publishes([
            __DIR__ . '/../database/migrations/create_audit_table.php' => database_path("migrations/{$timestamp}_create_audit_table.php"),
        ], 'migrations');
    }

    protected function registerConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/auditor.php',
            'auditor'
        );
    }
}
