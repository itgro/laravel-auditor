<?php
declare(strict_types=1);

return [

    'table' => 'audit',

    'enabled' => env('AUDITOR_ENABLED', false),

    'models_namespace' => 'App',

    'metadata' => [
        'env' => true,
        'running_in_console' => true,
        'user_id' => true,
        'user_ip' => false,
    ],

    'ignore_empty_saves' => false,

    'json_encode_options' => 0,

];
